﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW6
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        private void HandleMapOpen(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new MapPage());
        }

    }
}
